#pragma once

#include "Graph.hpp"
#include "Matrix.hpp"
#include <string>


template<typename T>
class DirGraph: public Graph<T>{

    public:

        DirGraph(int num_nodes): Graph<T>(num_nodes){};

        void SetWeight(T weight, int node1, int node2);
        ArraySequence<int>* TopologicalSort();
        void TopSortHelp(int v, bool visited[], ArraySequence<int>* stack);
        string GetGraphType(){return "DirGraph";}

};

template<typename T>
void DirGraph<T>::SetWeight(T weight, int node1, int node2){
    this->AdjMatrix->SetElement(weight, node1, node2);
}

template<typename T>
ArraySequence<int>* DirGraph<T>::TopologicalSort(){
    ArraySequence<int>* stack;
    stack = new ArraySequence<int>();
    //bool visited = new bool[this->GetSize()];

    //for(int i = 0; i < this->GetSize(); i++){visited[i] = false;}

    bool visited[this->GetSize()] = {0};

    for(int i = 0; i < this->GetSize(); i++){
        if (!visited[i]){this->TopSortHelp(i, visited, stack);}
    }

    return stack;
}

template<typename T>
void DirGraph<T>::TopSortHelp(int v, bool visited[], ArraySequence<int>* stack){
    visited[v] = true;

    for(int i = 0; i < this->GetSize(); i++){
        if(this->AdjMatrix->GetElement(v, i)){
            if(!visited[i]){this->TopSortHelp(i, visited, stack);}
        }
    }
    stack->Prepend(v);
}
