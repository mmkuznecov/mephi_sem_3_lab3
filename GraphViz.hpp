#include <fstream>
#include <cstdlib>
#include "Graph.hpp"
#include "DirGraph.hpp"
#include <typeinfo>


template<typename T, template<typename> class GrType>
void WriteDot(GrType<T> *g){
    ofstream out("g.dot");
    out << "digraph {\n";
    for (int i=0; i < g->GetSize(); i++){
        out << "  " << i << ";\n";
    }

    string gr_type = g->GetGraphType();

    if (gr_type == "DirGraph"){
        for (int i=0; i < g->GetSize(); i++){
            for (int j=0; j < g->GetSize(); j++){
                if (g->GetWeight(i, j)){
                    out << "  " << i << " -> " << j << "[label=\"" << g->GetWeight(i, j) << "\"]" << ";\n";
                }
            }
        }
    }

    else{
        for (int i=0; i < g->GetSize(); i++){
            for (int j=0; j < g->GetSize(); j++){
                if ((g->GetWeight(i, j)) && (j>=i)){
                    out << "  " << i << " -> " << j << "[dir=none, label=\"" << g->GetWeight(i, j) << "\"] " << ";\n";
                }
            }
        }
    }

    out << "}\n";

}


template<typename T, template<typename> class GrType>
void VizGraph(GrType<T> *g){
    WriteDot<T, GrType>(g);

    system("dot g.dot -Tpng -og.png");
    system("display g.png");
    system("rm -rf g.dot");
    system("rm -rf g.png");
}
