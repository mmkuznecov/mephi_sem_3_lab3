#pragma once

#include "Sequence.hpp"
#include "Matrix.hpp"
#include "ArraySequence.hpp"
#include "string"

using namespace std;

template <class T>
class Graph{

    protected:

        Matrix<T> *AdjMatrix;

    public:

        Graph();
        Graph(int num_nodes);
        ~Graph() = default;

        void AddNode();
        void RemoveNode(int node_index);

        void SetWeight(T weight, int node1, int node2);

        T GetWeight(int node1, int node2) const;
        ArraySequence<int>* GetPath(int start_node, int end_node) const;
        T GetPathWeight(ArraySequence<int>* path) const;
        int GetSize() const;
        void ShowGraph();
        string GetGraphType(){return "Graph";}
};


template <typename T>
Graph<T>::Graph(int num_nodes){
    this->AdjMatrix = new Matrix<T>(num_nodes, num_nodes);
}

template <typename T>
int Graph<T>::GetSize() const{
    return this->AdjMatrix->GetRows();
}

template <typename T>
void Graph<T>::SetWeight(T weight, int node1, int node2){
    this->AdjMatrix->SetElement(weight, node1, node2);
    this->AdjMatrix->SetElement(weight, node2, node1);
}

template <typename T>
T Graph<T>::GetWeight(int node1, int node2) const{
    return this->AdjMatrix->GetElement(node1, node2);
}

template<typename T>
T Graph<T>::GetPathWeight(ArraySequence<int>* path) const {
    T result = 0;

    for (int i = 0; i < path->GetLength() - 1; i++){
        result += this->AdjMatrix->GetElement(path->Get(i), path->Get(i + 1));
    }

    return result;
}

template<typename T>
ArraySequence<int>* Graph<T>::GetPath(int start_node, int end_node) const{
    int inf = 1e6;

    ArraySequence<int> dist(this->GetSize());
    ArraySequence<bool> beenTo(this->GetSize());
    ArraySequence<int> parent(this->GetSize());

    for (int i = 0; i < this->GetSize(); i++){
        dist.Set(i, inf);
        beenTo.Set(i, false);
        parent.Set(i, -1);
    }

    int min = 0;
    int index_min = 0;
    int temp = 0;
    dist.Set(start_node, 0);

    for (int i = 0; i < this->GetSize(); i++){
        min = inf;
        for (int j = 0; j < this->GetSize(); ++j){
            if (!beenTo.Get(j) && dist.Get(j) < min){
                min = dist.Get(j);
                index_min = j;
            }
        }
        beenTo.Set(index_min, true);


        for(int j = 0; j < this->GetSize(); ++j){
            if (this->AdjMatrix->GetElement(index_min, j) > 0){
                temp = min + this->AdjMatrix->GetElement(index_min, j);
                if (temp < dist.Get(j)){
                    dist.Set(j, temp);
                    parent.Set(j, index_min);
                }
            }
        }
        if(beenTo.Get(end_node)){break;}
    }

    ArraySequence<int> *path = new ArraySequence<int>(0);

    if(parent.Get(end_node) == -1){
        path->Append(-1);
        return path;
    }

    for(int i = end_node; i != -1; i = parent.Get(i)){
        path->Prepend(i);
    }

    return path;

}


template<typename T>
void Graph<T>::ShowGraph(){
    this->AdjMatrix->PrettyPrint();
}