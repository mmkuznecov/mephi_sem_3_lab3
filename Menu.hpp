#include <iostream>
#include <string>

#include "Graph.hpp"
#include "DirGraph.hpp"
#include "GraphAlgos.hpp"
#include "GraphViz.hpp"

using namespace GraphAlgos;

using namespace std;

string gr_type_options[3] = {"1--Undirected Graph", "2--Directed Graph", "3--Exit"};
string fill_option[2] = {"1--Default", "2--Custom"};
string undir_gr_options[6] = {"1--Edit Graph", "2--Find Path", "3--Find min spanning tree", "4--Color graph", "5--Visualize Graph", "6--Exit"};
string dir_gr_options[5] = {"1--Edit Graph", "2--Find Path", "3--Topological Sort", "4--Visualize Graph", "5--Exit"};
string edit_gr_options[2] = {"1--Edit", "2--Stop"};

string gr_type = "Choose type of graph:";
string fill_type = "Choose fill type:";
string undir = "Choose action with undirected graph:";
string dir = "Choose action with directed graph:";
string edit = "Graph Edition";


int GetOption(string MSGS[], int len, string description){

    cout << "=======================" << endl;

    cout << description << endl;

    for (int i = 0; i < len; i++){
        cout << MSGS[i] << endl;
    }

    cout << "=======================" << endl;

    int choice;

    cout << "Enter your choice: ";
    cin >> choice;

    return choice;
}


template<typename T, template<typename> class GrType>
GrType<T>* CreateDefaultGraph(){
    GrType<T>* gr = new GrType<T>(8);
    
    gr->SetWeight(5, 0, 1);
    gr->SetWeight(9, 0, 2);
    gr->SetWeight(3, 1, 2);
    gr->SetWeight(1, 1, 4);
    gr->SetWeight(2, 2, 3);
    gr->SetWeight(5, 3, 4);
    gr->SetWeight(4, 3, 7);
    gr->SetWeight(9, 4, 5);
    gr->SetWeight(8, 1, 5);
    gr->SetWeight(3, 5, 6);
    gr->SetWeight(4, 6, 7);
    return gr;
}

template<typename T, template<typename> class GrType>
void EditGraph(GrType<T>* graph){
    
    
    int edit_opt;

    edit_opt = GetOption(edit_gr_options, 2, edit);


    while(edit_opt-2){

        int n1, n2; 
        T w;
        cout << "Input start node: ";
        cin >> n1;
        cout << "Input end node: ";
        cin >> n2;
        cout << "Input weight: ";
        cin >> w;
        graph->SetWeight(w, n1, n2);

        edit_opt = GetOption(edit_gr_options, 2, edit);

    }
}

template<typename T, template<typename> class GrType>
GrType<T>* ManualInputGraph(){

    cout << "Input size of graph: ";
    int size;
    cin >> size;

    GrType<T>* graph = new GrType<T>(size);

    EditGraph<T, GrType>(graph);


    return graph;

}


void Run(int gr_choice, int fill_choice){
    switch(gr_choice){
        case 1:{

            Graph<int> * graph;

            switch(fill_choice){
                case 1:{
                    graph = CreateDefaultGraph<int, Graph>();
                    break;
                    }

                case 2:{
                    graph = ManualInputGraph<int, Graph>();
                    break;
                    }
                }

                int choice = 0;
                while(choice != 6){
                    choice = GetOption(undir_gr_options, 6, undir);
                    if (choice != 6){
                        switch(choice){
                            case 1:
                                {
                                EditGraph<int, Graph>(graph);
                                break;
                                }
                            case 2:
                                {
                                int start_node, end_node;
                                cout << "Input start node: ";
                                cin >> start_node;
                                cout << "Input end node: ";
                                cin >> end_node;
                                ArraySequence<int>* path = graph->GetPath(start_node, end_node);
                                cout << "Path: ";
                                path->PrettyPrint();
                                int path_sum_w = graph->GetPathWeight(path);
                                cout << "Path sum weight: " << path_sum_w << endl;
                                break;
                                }
                            case 3:{
                                cout << "Min spanning tree: " << endl;
                                PrimAlg(graph);
                                break;
                            }

                            case 4:{
                                cout << "Graph coloring: " << endl;
                                Coloring(graph);
                                break;
                            }

                            case 5:{
                                VizGraph<int, Graph>(graph);
                                break;
                            }

                            
                        }
                    }
                }

            break;
        }

        case 2:{

            DirGraph<int>* dgraph;

            switch(fill_choice){
                case 1:
                    dgraph = CreateDefaultGraph<int,DirGraph>();
                    break;
                
                case 2:
                    dgraph = ManualInputGraph<int,DirGraph>();
                    break;

            }

            int choice = 0;
            while(choice != 5){
                choice = GetOption(dir_gr_options, 5, dir);
                if (choice != 5){
                    switch(choice){
                        case 1:
                        {

                            EditGraph<int,DirGraph>(dgraph);
                            break;
                        }
                        case 2:
                        {
                            int start_node, end_node;
                            cout << "Input start node: ";
                            cin >> start_node;
                            cout << "Input end node: ";
                            cin >> end_node;
                            ArraySequence<int>* path = dgraph->GetPath(start_node, end_node);
                            cout << "Path: ";
                            path->PrettyPrint();
                            int path_sum_w = dgraph->GetPathWeight(path);
                            cout << "Path sum weight: " << path_sum_w << endl;
                            break;
                        }

                        case 3:
                            {

                            ArraySequence<int>* ts = dgraph->TopologicalSort();
                            cout << "Topological sort: ";
                            ts->PrettyPrint();
                            break;
                            }

                        case 4:{
                            VizGraph<int, DirGraph>(dgraph);
                            break;
                        }
                    }
                }
            }

            break;
        }
    }
}

void Menu(){

    int gr_choice = 0;
    gr_choice = GetOption(gr_type_options, 3, gr_type);


    if (gr_choice != 3){
        int fill_choice = 0;
        fill_choice = GetOption(fill_option, 2, fill_type);
        Run(gr_choice, fill_choice);
    }   
}