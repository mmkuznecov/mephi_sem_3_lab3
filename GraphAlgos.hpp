#include "Graph.hpp"
#include "DirGraph.hpp"



using namespace std;

namespace GraphAlgos{


    int minKey(int key[], bool mstSet[], int size){

        int min = 1e6;
        int min_index;
    
        for (int v = 0; v < size; v++)
            if (mstSet[v] == false && key[v] < min){
                min = key[v], 
                min_index = v;
        }

        return min_index;
    }

    template <typename T>
    void PrimAlg(Graph<T> * graph){
        
        int size = graph->GetSize();

        int parent[size];
        int key[size];
        bool mstSet[size];

        for(int i = 0; i < size; i++){
            key[i] = 1e6;
            mstSet[i] = false;
        }

        key[0] = 0;
        parent[0] = -1;

        for (int count = 0; count < size - 1; count++){
            int u = minKey(key, mstSet, size);

            mstSet[u] = true;

            for(int v = 0; v < size; v++){
                if(graph->GetWeight(u,v)!=0 && mstSet[v] == false && graph->GetWeight(u,v) < key[v]){
                    parent[v] = u;
                    key[v] = graph->GetWeight(u,v);
                }
            }
        }

        cout<<"Edge \tWeight\n";
        for (int i = 1; i < size; i++){
            cout<<parent[i]<<" - "<<i<<" \t"<<graph->GetWeight(i,parent[i])<<" \n";
        }



    }


    template<typename T>
    void Coloring(Graph<T>* graph){
        int size = graph->GetSize();

        int color[size] = {-1};
        color[0] = 0;

        bool colorUsed[size] = {false};

        for(int u = 1; u < size; u++){
            for(int v = 0; v < size; v++){
                if(graph->GetWeight(u,v)){
                    if(color[v] != -1){
                        colorUsed[color[v]] = true;
                    }
                }
            }
        

            int col;
            for(col = 0; col < size; col++){
                if(!colorUsed[col]){
                    break;
                }
            }

            color[u] = col;

            for (int v = 0; v < size; v++){
                if(graph->GetWeight(u,v)){
                    if(color[v] != -1){
                        colorUsed[color[v]] = false;
                    }
                }
            }
        }

        for(int u = 0; u<size; u++){
            cout <<"Color: " << u << ", Assigned with Color: " <<color[u] <<endl;
        }
    }

};