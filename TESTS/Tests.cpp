//
// Created by mikhail on 15.12.2021.
//

#include "gtest/gtest.h"
#include "../Graph.hpp"
#include "../DirGraph.hpp"
#include "../Matrix.hpp"

template<typename T, template<typename> class GrType>
GrType<T>* CreateDefaultGraph(){
    GrType<T>* gr = new GrType<T>(8);

    gr->SetWeight(5, 0, 1);
    gr->SetWeight(9, 0, 2);
    gr->SetWeight(3, 1, 2);
    gr->SetWeight(1, 1, 4);
    gr->SetWeight(2, 2, 3);
    gr->SetWeight(5, 3, 4);
    gr->SetWeight(4, 3, 7);
    gr->SetWeight(9, 4, 5);
    gr->SetWeight(8, 1, 5);
    gr->SetWeight(3, 5, 6);
    gr->SetWeight(4, 6, 7);
    return gr;
}

TEST(Matrix, Matrix){

    Matrix<int> *m = new Matrix<int>(2,2);

    m->SetElement(1, 0, 0);
    m->SetElement(2, 0, 1);
    m->SetElement(3, 1, 0);
    m->SetElement(4, 1, 1);

    ASSERT_EQ(1, m->GetElement(0,0));
    ASSERT_EQ(2, m->GetElement(0,1));
    ASSERT_EQ(3, m->GetElement(1,0));
    ASSERT_EQ(4, m->GetElement(1,1));

    m->SetElement(10, 0, 0);

    ASSERT_EQ(10, m->GetElement(0,0));

}

TEST(Graph, CheckFill){
    Graph<int> *gr = CreateDefaultGraph<int, Graph>();

    for (int i = 0; i < gr->GetSize(); i++){
        for (int j = 0; j < gr->GetSize(); j++){
            ASSERT_EQ(gr->GetWeight(i,j), gr->GetWeight(j,i));
        }
    }
}

TEST(Graph, PathTest){
    Graph<int> *gr = CreateDefaultGraph<int, Graph>();

    ArraySequence<int> *path = gr->GetPath(0,1);
    ASSERT_EQ(0, path->Get(0));
    ASSERT_EQ(1, path->Get(1));
}

TEST(DirGraph, CheckFill){
    DirGraph<int> *dgr = CreateDefaultGraph<int, DirGraph>();

    for (int i = 0; i < dgr->GetSize(); i++){
        for (int j = 0; j < dgr->GetSize(); j++){
            if(i!=j){
                ASSERT_FALSE(dgr->GetWeight(i, j ) == dgr->GetWeight(j, i));
            }
        }
    }

}

TEST(DirGraph, TopSortCheck){
    DirGraph<int>* dgr = CreateDefaultGraph<int, DirGraph>();

    ArraySequence<int> *ts = dgr->TopologicalSort();

    ASSERT_EQ(0, ts->Get(0));
    ASSERT_EQ(1, ts->Get(1));
    ASSERT_EQ(2, ts->Get(2));
    ASSERT_EQ(3, ts->Get(3));
    ASSERT_EQ(4, ts->Get(4));
    ASSERT_EQ(5, ts->Get(5));
    ASSERT_EQ(6, ts->Get(6));
    ASSERT_EQ(7, ts->Get(7));
}