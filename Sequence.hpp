//
// Created by mikhail on 09.11.2021.
//

#ifndef MEPHI_SEM_3_LAB2_SEQUENCE_HPP
#define MEPHI_SEM_3_LAB2_SEQUENCE_HPP

#pragma once

#include <iostream>

template <typename T>
class Sequence{
public:

    virtual int GetLength() const = 0;

    virtual T Get(int index) const = 0;

    virtual T& At(int index) = 0;

    virtual T GetLast() const = 0;

    virtual T GetFirst() const = 0;

    virtual Sequence<T>* GetSubSequence(int start, int end) = 0;

    virtual void Set(int index, T item) = 0;

    virtual Sequence<T>* Concat(Sequence<T>* list) = 0;

    virtual void Append(T item) = 0;

    virtual void Prepend(T item) = 0;

    virtual void InsertAt(T item, int index) = 0;

    virtual void PrettyPrint() = 0;

};

#endif //MEPHI_SEM_3_LAB2_SEQUENCE_HPP
