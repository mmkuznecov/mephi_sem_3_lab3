#ifndef MEPHI_SEM_3_LAB2_MATRIX_HPP
#define MEPHI_SEM_3_LAB2_MATRIX_HPP

#pragma once

#include "Sequence.hpp"
#include "DynamicArray.hpp"
#include "ArraySequence.hpp"
#include <iostream>


template<typename T>
class Matrix{
    
    private:

        Sequence<Sequence<T>*>* m_matrix;
        int m_rows = 0;
        int m_cols = 0;
    
    public:

        Matrix();
        Matrix(int rows, int cols);
        Matrix(T** data, int rows, int cols);
        ~Matrix() = default;

        T GetElement(int row, int col) const;
        int GetRows(){return this->m_rows;};
        int GetCols(){return this->m_cols;};

        void SetElement(T value, int row, int col);

        void PrettyPrint();
        void GetShape();

};

template<typename T>
Matrix<T>::Matrix(){
    m_matrix = NULL;
    this->m_rows = 0;
    this->m_cols = 0;
}


template<typename T>
Matrix<T>::Matrix(int rows, int cols){
    
    this->m_rows = rows;
    this->m_cols = cols;

    this->m_matrix = (Sequence<Sequence<T>*>*) new ArraySequence<ArraySequence<T>*>(this->m_rows);
    for (int i = 0; i < this->m_rows; i++)
    {
        this->m_matrix->Set(i, new ArraySequence<T>(this->m_cols));
    }
}


template<typename T>
Matrix<T>::Matrix(T** data, int rows, int cols): Matrix<T>(rows, cols){

    for (int i = 0; i < rows; i++){
        for (int j = 0; j < cols; j++){
            this->m_matrix->At(i)->Set(j, data[i][j]);
        }
    }
}

template<typename T>
T Matrix<T>::GetElement(int row, int col) const {
    return this->m_matrix->Get(row)->Get(col);
}

template<typename T>
void Matrix<T>::SetElement(T value, int row, int col){
    this->m_matrix->At(row)->Set(col, value);
}


template<typename T>
void Matrix<T>::PrettyPrint(){

    for(int i = 0; i < this->m_rows; i++){
        for(int j = 0; j < this->m_cols; j++){
            cout << this->m_matrix -> Get(i) -> Get(j) << "\t";
        }
        cout << endl;
    }
}

template<typename T>
void Matrix<T>::GetShape(){
    cout << "(" << this->m_rows << "," << this->m_cols << ")" << endl;
}


#endif // MEPHI_SEM_3_LAB2_MATRIX_HPP